#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void print(int *x_array, int *y_array, int x_size, int y_size) {
    printf("Width: %d\nHeight: %d\n", x_size, y_size);
    printf(" ");
    for (size_t i = 0; i < x_size; i++) {
        printf("%d ", x_array[i]);
    }
    printf("\n");
    for (size_t i = 0; i < y_size; i++) {
        printf("%d\n", y_array[i]);
    }
}

int cut(int *x_array, int *y_array, int x_size, int y_size) {
    //print(x_array, y_array, x_size, y_size);
    //Make sure there's actual cuts to be made
    //If there isn't on one side then there's no decision making to be made:
    //as cutting on one axis doesn't affect other values on the same axis. So
    //we just "cut" all of them and return their value.
    if (x_size == 0) {
        int y_sum = 0;
        for (size_t i = 0; i < y_size; i++) {
            y_sum += y_array[i];
        }
        return y_sum;
    }
    if (y_size == 0) {
        int x_sum = 0;
        for (size_t i = 0; i < x_size; i++) {
            x_sum += x_array[i];
        }
        return x_sum;
    }

    //Find right place to cut
    //Eventually maybe sort the arrays first
    //Then find the biggest cost so we can cut it now instead of later
    int highest_x_index = 0;
    for (size_t i = 0; i < x_size; i++) {
        if (x_array[i] > x_array[highest_x_index]) {
            highest_x_index = i;
        }
    }

    int highest_y_index = 0;
    for (size_t i = 0; i < y_size; i++) {
        if (y_array[i] > y_array[highest_y_index]) {
            highest_y_index = i;
        }
    }
    

    //printf("Highest x: %d\n", x_array[highest_x_index]);
    //printf("Highest y: %d\n", y_array[highest_y_index]);

    //Now that we have the highest index of each, we see which is best compared
    //to the sum of the other group. As cutting on one side doubles the required
    //cuts on the other side. We choose the lowest difference.
    if (x_array[highest_x_index] > y_array[highest_y_index]) {
        //We're going with cutting vertically on one of the x values.
             //Left part, includes everything left of highest_x_index
        return cut(x_array, y_array, highest_x_index, y_size)
            //Right part, everything right of highest_x_index
            + cut(x_array + (highest_x_index+1), y_array, x_size - (highest_x_index+1), y_size)
            //Cost of cutting highest_x_index
            + x_array[highest_x_index];
    } else {
        //We're going with horizontal y.
             //Top part, includes everything above of highest_y_index
        return cut(x_array, y_array, x_size, highest_y_index)
            //Bottom part, everything below of highest_y_index
            + cut(x_array, y_array + (highest_y_index+1), x_size, y_size - (highest_y_index+1))
            //Cost of cutting highest_y_index
            + y_array[highest_y_index];
    }
}

int main() {
    int m, n;
    scanf("%d %d", &m, &n);
    int x_size = m - 1;
    int y_size = n - 1;

    int *x_array = malloc(x_size*4);
    int *y_array = malloc(y_size*4);

    for (size_t i = 0; i < x_size; i++) {
        int value;
        scanf("%d", &value);
        x_array[i] = value;
    }

    for (size_t i = 0; i < y_size; i++) {
        scanf("%d", &y_array[i]);
    }

    printf("%d\n", cut(x_array, y_array, x_size, y_size));
}
